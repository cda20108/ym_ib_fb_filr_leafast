package fr.afpa.leafast.controller;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import fr.afpa.beans.Admin;
import fr.afpa.beans.Annonce;
import fr.afpa.beans.Compte;
import fr.afpa.beans.Livreur;
import fr.afpa.beans.Particulier;
import fr.afpa.repositories.dao.AdminRepository;
import fr.afpa.repositories.dao.AnnonceRepository;
import fr.afpa.repositories.dao.LivreurRepository;
import fr.afpa.repositories.dao.ParticulierRepository;

@Controller
public class AdminController {

	@Autowired
	AnnonceRepository ap;

	@Autowired
	AdminRepository ar;

	@Autowired
	ParticulierRepository pr;

	@Autowired
	LivreurRepository lr;

	@GetMapping(value = "/connexionA")
	public String redirectCoForm() {
		return "connexionAdmin";
	}

	@GetMapping(value = "/inscriptionA")
	public String redirectForm() {
		return "creationAdmin";
	}

	@GetMapping(value = "/demandeMenuAdmin")
	public String redirectMenuForm(Model model) {

		Integer nbrUser = 0;
		Integer nbrPar = 0;
		Integer nbrLiv = 0;
		Integer nbrAnn = 0;

		ArrayList<Annonce> listeAnnonce = (ArrayList<Annonce>) ap.findAll();

		model.addAttribute("listeAnn", listeAnnonce);

		ArrayList<Particulier> listeParticulier = (ArrayList<Particulier>) pr.findAll();

		for (int i = 0; i < listeParticulier.size(); i++) {
			nbrPar++;
		}

		ArrayList<Livreur> listeLivreur = (ArrayList<Livreur>) lr.findAll();

		model.addAttribute("listeLiv", listeLivreur);

		for (int i = 0; i < listeLivreur.size(); i++) {
			nbrLiv++;
		}

		nbrUser = nbrPar + nbrLiv;

		ArrayList<Annonce> nbrAnnonces = (ArrayList<Annonce>) ap.findAll();

		for (int i = 0; i < nbrAnnonces.size(); i++) {
			nbrAnn++;
		}

		model.addAttribute("nbrUser", nbrUser);
		model.addAttribute("nbrPar", nbrPar);
		model.addAttribute("nbrLiv", nbrLiv);
		model.addAttribute("nbrAnn", nbrAnn);

		return "menuAdmin";
	}

	@GetMapping(value = "/contactUS")
	public String redirectUSForm() {
		return "adminNousContacter";
	}

	@PostMapping(value = "/creationAdmin")
	public ModelAndView ajoutAdmin(ModelAndView mv, @RequestParam(value = "nom") String nom,
			@RequestParam(value = "prenom") String prenom, @RequestParam(value = "mail") String mail,
			@RequestParam(value = "tel") String tel, @RequestParam(value = "dateNaissance") String date,
			@RequestParam(value = "login") String login, @RequestParam(value = "mdp") String mdp) {

		LocalDate dateNaissance = LocalDate.parse(date);

		Admin admin = new Admin(nom, prenom, mail, tel, dateNaissance);

		String mdpHash = BCrypt.hashpw(mdp, BCrypt.gensalt());

		Compte compte = new Compte(login, mdpHash);

		admin.setCompte(compte);

		ar.save(admin);

		ar.flush();

		mv.setViewName("home");

		return mv;
	}

	@GetMapping(value = "/modifierAdmin")
	public ModelAndView modifierAdmin(ModelAndView mv, @RequestParam(value = "id") Integer id) {

		Admin admin = new Admin();

		admin = ar.findById(id).get();

		mv.addObject("admin", admin);

		mv.setViewName("adminModifierInfos");

		return mv;
	}

	@PostMapping(value = "/modifierAdmin")
	public ModelAndView modifierAdmin(ModelAndView mv, @RequestParam(value = "idA") Integer idA,
			@RequestParam(value = "nom") String nom, @RequestParam(value = "prenom") String prenom,
			@RequestParam(value = "mail") String mail, @RequestParam(value = "tel") String tel,
			@RequestParam(value = "date") String date, @RequestParam(value = "idC") Integer idC,
			@RequestParam(value = "login") String login, @RequestParam(value = "mdp") String mdp) {

		LocalDate dateNaissance = LocalDate.parse(date);

		String mdpHash = BCrypt.hashpw(mdp, BCrypt.gensalt());

		Compte compte = new Compte();

		compte.setIdCompte(idC);
		compte.setLogin(login);
		compte.setMdp(mdpHash);

		Admin admin = new Admin();

		admin.setIdAdmin(idA);
		admin.setNom(nom);
		admin.setPrenom(prenom);
		admin.setMail(mail);
		admin.setTel(tel);
		admin.setDateNaissance(dateNaissance);
		admin.setCompte(compte);

		ar.saveAndFlush(admin);

		mv.addObject("admin", admin);

		mv.setViewName("adminModifierInfos");

		return mv;
	}

	@GetMapping(value = "/adListeAnnonces")
	public ModelAndView listeAnnonces(ModelAndView mv) {

		ArrayList<Annonce> liste = (ArrayList<Annonce>) ap.findAll();

		mv.addObject("listeann", liste);
		
		ArrayList<Livreur> listeLivreur = (ArrayList<Livreur>) lr.findAll();

		mv.addObject("listeLiv", listeLivreur);

		mv.setViewName("adminListeAnnonces");

		return mv;
	}

	@GetMapping(value = "/adListeParticuliers")
	public ModelAndView listeParticuliers(ModelAndView mv) {

		ArrayList<Particulier> liste = (ArrayList<Particulier>) pr.findAll();

		mv.addObject("listepar", liste);

		mv.setViewName("adminListeParticuliers");

		return mv;
	}

	@GetMapping(value = "/adListeLivreurs")
	public ModelAndView listeLivreurs(ModelAndView mv) {

		ArrayList<Livreur> liste = (ArrayList<Livreur>) lr.findAll();

		mv.addObject("listeliv", liste);

		mv.setViewName("adminListeLivreurs");

		return mv;
	}

	@GetMapping(value = "/adminSupprimerParticulier")
	public ModelAndView adminSupprimerParticulier(ModelAndView mv, @RequestParam(value = "id") String id) {

		pr.deleteById(Integer.parseInt(id));

		ArrayList<Particulier> liste = (ArrayList<Particulier>) pr.findAll();

		mv.addObject("listepar", liste);

		mv.setViewName("adminListeParticuliers");

		return mv;
	}

	@GetMapping(value = "/adminSupprimerLivreur")
	public ModelAndView adminSupprimerLivreur(ModelAndView mv, @RequestParam(value = "id") String id) {

		lr.deleteById(Integer.parseInt(id));

		ArrayList<Livreur> liste = (ArrayList<Livreur>) lr.findAll();

		mv.addObject("listeliv", liste);

		mv.setViewName("adminListeLivreurs");

		return mv;
	}

	@GetMapping(value = "/adminSupprimerAnnonce")
	public ModelAndView adminSupprimerAnnonce(ModelAndView mv, @RequestParam(value = "id") String id) {

		ap.deleteById(Integer.parseInt(id));

		ArrayList<Annonce> liste = (ArrayList<Annonce>) ap.findAll();

		mv.addObject("listeann", liste);

		mv.setViewName("adminListeAnnonces");

		return mv;
	}

	@GetMapping(value = "/profilAdmin")
	public ModelAndView profilAdmin(ModelAndView mv, @RequestParam(value = "id") Integer id) {

		Admin admin = new Admin();

		admin = ar.findById(id).get();

		mv.addObject("admin", admin);

		mv.setViewName("adminProfil");

		return mv;
	}

	@PostMapping(value = "/contactUS")
	public String contactUS(Model model, @RequestParam(value = "car") String car,
			@RequestParam(value = "login") String login, @RequestParam(value = "nom") String nom,
			@RequestParam(value = "prenom") String prenom, @RequestParam(value = "tel") String tel,
			@RequestParam(value = "mail") String mail, @RequestParam(value = "descriptif") String desc) {

		String username = "";
		String password = "";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		Message message = new MimeMessage(session);

		try {

			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("fouadbouchlaghem@hotmail.com"));
			message.setSubject("Contact " + login);
			message.setText("Personne : " + car + "Nom : " + nom + "Prenom : " + prenom + "T�l�phone : " + tel
					+ "Mail : " + mail + "Message : " + desc);

			Transport.send(message);

		} catch (AddressException e) {

			e.printStackTrace();
		} catch (MessagingException e) {

			e.printStackTrace();
		}

		Integer nbrUser = 0;
		Integer nbrPar = 0;
		Integer nbrLiv = 0;
		Integer nbrAnn = 0;

		ArrayList<Annonce> listeAnnonce = (ArrayList<Annonce>) ap.findAll();

		model.addAttribute("listeAnn", listeAnnonce);

		ArrayList<Particulier> listeParticulier = (ArrayList<Particulier>) pr.findAll();

		for (int i = 0; i < listeParticulier.size(); i++) {
			nbrPar++;
		}

		ArrayList<Livreur> listeLivreur = (ArrayList<Livreur>) lr.findAll();

		for (int i = 0; i < listeLivreur.size(); i++) {
			nbrLiv++;
		}

		nbrUser = nbrPar + nbrLiv;

		ArrayList<Annonce> nbrAnnonces = (ArrayList<Annonce>) ap.findAll();

		for (int i = 0; i < nbrAnnonces.size(); i++) {
			nbrAnn++;
		}

		model.addAttribute("nbrUser", nbrUser);
		model.addAttribute("nbrPar", nbrPar);
		model.addAttribute("nbrLiv", nbrLiv);
		model.addAttribute("nbrAnn", nbrAnn);

		return "menuAdmin";
	}

	public ModelAndView ajoutImage(@RequestParam CommonsMultipartFile file, HttpSession session) {

		String path = session.getServletContext().getRealPath("/");

		String filename = file.getOriginalFilename();

		System.out.println(path + " " + filename);

		try {

			byte barr[] = file.getBytes();

			BufferedOutputStream bout = new BufferedOutputStream(

					new FileOutputStream(path + "/" + filename));

			bout.write(barr);
			bout.flush();
			bout.close();

		} catch (Exception e) {
			System.out.println(e);
		}
		return new ModelAndView("adminProfil", "filename", path + "/" + filename);
	}
}
