package fr.afpa.leafast.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.Particulier;
import fr.afpa.repositories.dao.AnnonceRepository;
import fr.afpa.repositories.dao.ParticulierRepository;

@Controller
@SessionAttributes("particulier")
public class AuthentificationParticulier {

	@Autowired
	ParticulierRepository pr;
	@Autowired
	AnnonceRepository ap;

	@GetMapping(value = "/singin")
	public String singIn(Model model, @RequestParam(value = "login") String login,
			@RequestParam(value = "mdp") String mdp) {

		ArrayList<Particulier> listeParticulier = (ArrayList<Particulier>) pr.findAll();

		if (listeParticulier != null) {
			for (Particulier particulier : listeParticulier) {
				if (particulier.getCompte().getLogin().equals(login)
						&& BCrypt.checkpw(mdp, particulier.getCompte().getMdp())) {
					model.addAttribute("particulier", particulier);
					
					ArrayList<Annonce> listeAnnonce = (ArrayList<Annonce>) ap.findAll();
					
					ArrayList<Annonce> listeAnnonceP = new ArrayList<Annonce>();
					int nbrAnnonce = 0;
					int nbrAnnonceEnCours = 0;
					double argentGagner = 0;
					for (Annonce annonce : listeAnnonce) {
						nbrAnnonce += 1;
						
						if (annonce.getIsActive().equals("En Cours")) {
							
							nbrAnnonceEnCours += 1;
							
						}
					}
					argentGagner = nbrAnnonce * 10 * 0.24;
					model.addAttribute("nbrAnnonceEnCours",nbrAnnonceEnCours);
					model.addAttribute("argentGagner",argentGagner);
					model.addAttribute("nbrAnnonce",nbrAnnonce);
					model.addAttribute("listeAnn",listeAnnonce);
					
					return "menuParticulier";
					
				}
			}
		}

		return "home";
	}

	@GetMapping(value ="/voirAnnonceP/{i}/{j}")
	public ModelAndView voirAnnonceP(@PathVariable int i, @PathVariable int j) {
		ArrayList<Annonce> listeAnnonce = (ArrayList<Annonce>) ap.findAll(PageRequest.of(i, j));
		ModelAndView mv = new ModelAndView();
		mv.addObject("listAnn", listeAnnonce);
		return mv;
	}
	@GetMapping(value = "/singout")
	public String singOut(HttpSession session, SessionStatus status) {

		session.removeAttribute("particulier");
		status.setComplete();

		return "redirect:/";
	}
}
