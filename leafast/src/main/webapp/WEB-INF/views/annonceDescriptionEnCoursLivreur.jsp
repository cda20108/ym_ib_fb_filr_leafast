<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Annonce en cours livreur</title>
</head>
<body>
	<h1>ANNONCE EN COURS PAR VOUS MEME</h1>
	<p>Titre : ${ annonce.nomAnnonce}</p>
	<p>Ville destinataire : ${ annonce.adresseDepart.nomRue} ${ annonce.adresseDepart.codePostal} ${ annonce.adresseDepart.ville}</p>
	<p>Ville destinataire : ${ annonce.adresseArrive.nomRue} ${ annonce.adresseArrive.codePostal} ${ annonce.adresseArrive.ville}</p>
	<p>Taille du colis : ${ annonce.taille_colis} cm</p>
	<p>Prix : ${ annonce.prix_total} euros</p>
	<p>Date de livraison : ${ annonce.date_limite}</p>
	<p>Description : ${ annonce.descriptif}</p>
	<p>Etat : ${ annonce.isActive} par ${ livreur.nom} </p>
	
	<form action="abandonAnnonce" method="post">
	
	<input type="text" value="${ livreur.idLivreur}" name="idLivreur" hidden="true">
	<input type="text" value="${ annonce.idAnnonce}" name="idAnnonce" hidden="true">
	
	<button type="submit">Abandonner cette annonce </button>
	</form>
	
	<form action="valideAnnonce" method="post">
	
	<input type="text" value="${ livreur.idLivreur}" name="idLivreur" hidden="true">
	<input type="text" value="${ annonce.idAnnonce}" name="idAnnonce" hidden="true">
	<button type="submit">Annonce fini </button>
	</form>
</body>
</html>