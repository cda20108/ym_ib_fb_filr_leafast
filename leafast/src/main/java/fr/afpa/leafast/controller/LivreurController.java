package fr.afpa.leafast.controller;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.Compte;
import fr.afpa.beans.Vehicule;
import fr.afpa.beans.Livreur;
import fr.afpa.beans.Particulier;
import fr.afpa.repositories.dao.AnnonceRepository;
import fr.afpa.repositories.dao.LivreurRepository;
import fr.afpa.repositories.dao.ParticulierRepository;

@Controller
public class LivreurController {

	@Autowired
	LivreurRepository lr;
	@Autowired
	AnnonceRepository ap;
	@Autowired
	ParticulierRepository pr;
	
	@GetMapping(value = "/inscription2")
	public ModelAndView redirectForm(ModelAndView mv) {
		
		return listeVehicule(mv);
	}

	@GetMapping(value = "/ajouterVehicule")
	public String redirectVForm() {
		return "ajoutVehicule";
	}

	@GetMapping(value = "/connexionL")
	public String redirectCoForm() {
		return "connexionLiv";
	}

	@PostMapping(value = "/inscriptionL")
	public ModelAndView ajoutLivreur(ModelAndView mv, @RequestParam(value = "nom") String nom,
			@RequestParam(value = "prenom") String prenom, @RequestParam(value = "mail") String mail,
			@RequestParam(value = "tel") String tel, @RequestParam(value = "date") String date,
			@RequestParam(value = "login") String login, @RequestParam(value = "mdp") String mdp,
			@RequestParam(value = "typeVehicule") String type, @RequestParam(value = "marqueVehciule") String marque,
			@RequestParam(value = "modelVehicule") String model, @RequestParam(value = "emission") Integer emission,
			@RequestParam(value = "plaque") String plaque) {
		

		LocalDate dateNaissance = LocalDate.parse(date);

		Float note = (float) 0;

		Integer nbrNote = 0;

		String mdpHash = BCrypt.hashpw(mdp, BCrypt.gensalt());

		Livreur livreur = new Livreur(nom, prenom, mail, tel, dateNaissance, note, nbrNote, (float) 0);

		Compte compte = new Compte(login, mdpHash);

		livreur.setCompte(compte);

		Vehicule vehicule = new Vehicule(type, marque, model, emission, plaque);

		livreur.setVehicule(vehicule);

		lr.saveAndFlush(livreur);

		mv.addObject("livreur", livreur);

		mv.setViewName("connexionLiv");

		return mv;
	}

	@GetMapping(value = "/modifierLivreur")
	public ModelAndView modifierLivreur(ModelAndView mv, @RequestParam(value = "id") Integer id) {

		Livreur livreur = new Livreur();

		livreur = lr.findById(id).get();

		mv.addObject("livreur", livreur);

		mv.setViewName("modifierInfosLivreur");

		return mv;
	}

	@PostMapping(value = "/modifierLivreur")
	public ModelAndView modifierLivreur(ModelAndView mv, @RequestParam(value = "idL") Integer idL,
			@RequestParam(value = "idC") Integer idC, @RequestParam(value = "idV") Integer idV,
			@RequestParam(value = "nom") String nom, @RequestParam(value = "prenom") String prenom,
			@RequestParam(value = "mail") String mail, @RequestParam(value = "tel") String tel,
			@RequestParam(value = "date") String date, @RequestParam(value = "login") String login,
			@RequestParam(value = "mdp") String mdp, @RequestParam(value = "typeVehicule") String type,
			@RequestParam(value = "marqueVehciule") String marque, @RequestParam(value = "modelVehicule") String model,
			@RequestParam(value = "emission") Integer emission, @RequestParam(value = "plaque") String plaque) {

		LocalDate dateNaissance = LocalDate.parse(date);

		String mdpHash = BCrypt.hashpw(mdp, BCrypt.gensalt());

		Livreur livreur = lr.findById(idL).get();

		livreur.getCompte().setMdp(mdpHash);


		livreur.getVehicule().setTypeVehicule(type);
		livreur.getVehicule().setMarqueVehicule(marque);
		livreur.getVehicule().setModelVehicule(model);
		livreur.getVehicule().setEmission(emission);
		livreur.getVehicule().setPlaque(plaque);


		livreur.setIdLivreur(idL);
		livreur.setNom(nom);
		livreur.setPrenom(prenom);
		livreur.setEmail(mail);
		livreur.setTel(tel);
		livreur.setDateNaissance(dateNaissance);

		lr.saveAndFlush(livreur);
		
		ArrayList<Annonce> listeAnnonce = (ArrayList<Annonce>) ap.findAll();
		
		ArrayList<Annonce> listeAnnonceP = new ArrayList<Annonce>();
		
		int nbrAnnonce = 0;
		int nbrAnnonceEnCours = 0;
		for (Annonce annonce : listeAnnonce) {
			nbrAnnonce += 1;
			if (annonce.getId_LivreurEnCharge() == livreur.getIdLivreur()) {
				
				listeAnnonceP.add(annonce);
				
			}
			if (annonce.getId_LivreurEnCharge() == livreur.getIdLivreur() & annonce.getIsActive().equals("En Cours")) {
				
				nbrAnnonceEnCours += 1;
				
			}
		}
		
		ArrayList<Particulier> listeparticulier = (ArrayList<Particulier>) pr.findAll();
		
		int nbrParticulier = 0;
		for (Particulier particulier : listeparticulier) {
			nbrParticulier += 1;
		
		}
		
		mv.addObject("nbrAnnonceEnCours",nbrAnnonceEnCours);
		mv.addObject("nbrAnnonce",nbrAnnonce);
		mv.addObject("nbrParticulier",nbrParticulier);
		mv.addObject("listeAnn",listeAnnonceP);

		mv.addObject("livreur", livreur);

		mv.setViewName("menuLivreur");

		return mv;
	}

	public ModelAndView listeVehicule(ModelAndView mv) {
		 BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader("C:\\env\\workspace\\ym_ib_fb_filr_leafast\\leafast\\src\\main\\webapp\\resources\\csv\\car_serie.csv"));
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		    String ligne = null;
		    List  carList = new ArrayList <>();
		    try {
				while ((ligne = br.readLine()) != null)
				 {
				  // Retourner la ligne dans un tableau
				  String[] data = ligne.split(",");
				 
				  // Afficher le contenu du tableau
   

				  while ((ligne = br.readLine()) != null) {
				      String[] temp = ligne.split(",");
				      String marque = temp[0];
				    
				      carList.add(marque);
				  }
				  
				  for(int i = 0 ; i < carList.size(); i++) {
					   System.out.println(carList.get(i));
				 
				}}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    try {
				br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    mv.addObject("carlist", carList);

			
		    mv.setViewName("inscriptionLivreur");
			return mv;
		
		
	}
}
