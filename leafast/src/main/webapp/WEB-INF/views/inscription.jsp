<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<script src="${pageContext.request.contextPath}/resources/js/autocompleteInscription.js"></script>
	<script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
	<script src="https://code.jquery.com/jquery-3.5.0.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/animate.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/style.css">

<!-- Google Fonts -->
<link
	href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700|Lato:400,100,300,700,900'
	rel='stylesheet' type='text/css'>



<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<meta charset="ISO-8859-1">
<title>Inscription</title>
</head>
<body>
	<form action="inscription" method="post">
		<div class="container">
			<div class="top">
				<div class="logo">
					  <img src="${pageContext.request.contextPath}/resources/images/logo_transparent.png" width="250" height="250" alt="">
				</div>
				<h1 id="title" class="hidden">
					<span id="logo"><span>Une livraison plus sur pour
							vous et l'environnement</span></span>
				</h1>
			</div>
			<div class="login-box animated fadeInUp">
				<div class="box-header">
					<h2>Inscription particulier</h2>
				</div>
				<label for="username">Nom</label> <br /> <input type="text"
					placeholder="Nom" name="nom" required pattern='[A-z]{1,40}'>
				<br /> 
				<label for="username">Prenom</label> <br /> <input
					type="text" placeholder="Prenom" name="prenom" required
					pattern='[A-z]{1,40}'> <br />
					<label for="username">E-mail</label>
				<br /> <input type="email" placeholder="Mail" name="mail" required pattern='{5,50}'>
				<br />
				<label for="username">Telephone </label> <br /> <input
					type="tel" placeholder="T�l" name="tel" required pattern='[0-9]{10}'> <br />
					<label
					for="username">date de naissance</label> <br /> <input type="date"
					placeholder="Date de naissance" name="date"> <br />
					
					<label
					for="username">Nom de rue</label> 
					<br /> 
					<input type="text"
					placeholder="Nom de rue" name="nomRue" id="ship-address" required pattern="[a-z\ ][A-Z][']{1,50}"> <br />
					<label
					for="username">Code Postale</label> <br /> <input type="text" 
					placeholder="Code postal" name="codeP" id="postcode" required pattern='[0-9]{5}'> <br />
					<label
					for="username">Ville</label> <br /> <input type="text"
					placeholder="Ville" name="ville" id="locality" required pattern='[A-z]{1,20}'> <br />
					<label
					for="username">Login de connexion</label> <br /> <input
					type="text" placeholder="Login" name="login" required pattern='[A-z]{5,20}'> <br />
					<label
					for="username">Mot de passe</label> <br /> <input type="password"
					placeholder="Mot de passe" name="mdp" required pattern='{5,20}'> <br />

				<button type="submit">VALIDER</button>
				</div>
			</div>
		</form>
		
</body>
<script>
	$(document).ready(function() {
		$('#logo').addClass('animated fadeInDown');
		$("input:text:visible:first").focus();
	});
	$('#username').focus(function() {
		$('label[for="username"]').addClass('selected');
	});
	$('#username').blur(function() {
		$('label[for="username"]').removeClass('selected');
	});
	$('#password').focus(function() {
		$('label[for="password"]').addClass('selected');
	});
	$('#password').blur(function() {
		$('label[for="password"]').removeClass('selected');
	});
</script>

 <script
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcGNJDusE2KMpd3BXVWFoh9qsxTp7jROQ&callback=initAutocomplete&libraries=places&v=weekly"
      async
    ></script>

</html>