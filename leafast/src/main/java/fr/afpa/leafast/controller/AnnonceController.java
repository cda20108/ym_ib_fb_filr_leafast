package fr.afpa.leafast.controller;

import java.net.URI;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Properties;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.afpa.beans.Adresse;
import fr.afpa.beans.Annonce;
import fr.afpa.beans.Livreur;
import fr.afpa.beans.Particulier;
import fr.afpa.repositories.dao.AnnonceRepository;
import fr.afpa.repositories.dao.LivreurRepository;
import fr.afpa.repositories.dao.ParticulierRepository;

@Controller
public class AnnonceController {

	@Autowired
	AnnonceRepository ap;
	@Autowired
	ParticulierRepository pr;
	@Autowired
	LivreurRepository lr;
	
	@GetMapping(value = "/creationAnnonce")
	public ModelAndView redirectForm(ModelAndView mv, @RequestParam(value = "id") Integer id) {
		

		Particulier particulier = pr.findById(id).get();

		mv.addObject("particulier", particulier);
		
		
		
		mv.setViewName("creationAnnonce");
		
		return mv;
	}
	
	
	

	@PostMapping(value = "/creationAnnonce")
	public ModelAndView creerAnnonce(ModelAndView mv,@RequestParam(value = "id") Integer idP,@RequestParam(value = "nom_annonce") String nom_annonce,
			@RequestParam(value = "ship-address") String address1, @RequestParam(value = "locality") String locality,
			@RequestParam(value = "postcode") String postcode,
			@RequestParam(value = "ship-address2") String address2, @RequestParam(value = "locality2") String locality2,
			@RequestParam(value = "postcode2") String postcode2,
			@RequestParam(value = "km") Float km,
			
			@RequestParam(value = "taille_colis") String taille_colis,
			@RequestParam(value = "prix_km") String prix_km, @RequestParam(value = "date_limite") String date_limite
			, @RequestParam(value = "descriptif") String descriptif) {

		LocalDate date = LocalDate.parse(date_limite);
		
		
		Float convPrix = Float.parseFloat(prix_km);
		float kilometre = km / 1000;
		Float prix_total = kilometre * convPrix;

		Adresse adresse1 = new Adresse(address1, postcode, locality);
		Adresse adresse2 = new Adresse(address2, postcode2, locality2);
		
		Annonce annonce = new Annonce(nom_annonce, adresse1, adresse2, taille_colis, prix_km, km, prix_total, date, descriptif,"Valide");
		
		Particulier particulier = new Particulier();

		particulier = pr.findById(idP).get();
		
		annonce.setParticulier(particulier);
		
		ap.saveAndFlush(annonce);

		
		pr.saveAndFlush(particulier);

		mv.addObject("annonce", annonce);
		

		mv.addObject("particulier", particulier);
		
		ArrayList<Annonce> listeAnnonce = (ArrayList<Annonce>) ap.findAll();
		
		mv.addObject("listeAnn",listeAnnonce);

		mv.setViewName("menuParticulier");

		return mv;
	}
	
	
	@GetMapping(value = "/modifierAnnonceParticulier")
	public ModelAndView redirectModification(ModelAndView mv, @RequestParam(value = "idAnnonce") Integer idAnnonce
			, @RequestParam(value = "idParticulier") Integer idParticulier) {
		System.out.print(idAnnonce);
		System.out.print(idParticulier);

		

		Annonce annonce = ap.findById(idAnnonce).get();
		Particulier particulier = pr.findById(idParticulier).get();
		

		mv.addObject("annonce", annonce);
		mv.addObject("particulier", particulier);
		
		
		mv.setViewName("modifAnnonce");
		
		return mv;
	}
	
	
	@PostMapping(value = "/modifAnnonce")
	public ModelAndView modifierAnnonce (ModelAndView mv, @RequestParam(value = "nom_annonce") String nom_annonce,
	@RequestParam(value = "ship-address") String address1, @RequestParam(value = "locality") String locality,
	@RequestParam(value = "postcode") String postcode,
	@RequestParam(value = "ship-address2") String address2, @RequestParam(value = "locality2") String locality2,
	@RequestParam(value = "postcode2") String postcode2,
	@RequestParam(value = "km") Float km,
	
	@RequestParam(value = "taille_colis") String taille_colis,
	@RequestParam(value = "prix_km") String prix_km, @RequestParam(value = "date_limite") String date_limite
	, @RequestParam(value = "descriptif") String descriptif, @RequestParam(value = "isActive") String isActive
	, @RequestParam(value = "id") Integer idParticulier, @RequestParam(value = "idAnnonce") Integer idAnnonce) {
		
		LocalDate date = LocalDate.parse(date_limite);
		
		Float convPrix = Float.parseFloat(prix_km);
		float kilometre = km / 1000;
		Float prix_total = kilometre * convPrix;
		
		Adresse adresse1 = new Adresse(address1, postcode, locality);
		Adresse adresse2 = new Adresse(address2, postcode2, locality2);
		Annonce annonce = new Annonce(nom_annonce, adresse1, adresse2, taille_colis, prix_km, km, prix_total, date, descriptif,isActive);
		
		Particulier particulier = pr.findById(idParticulier).get();
		
		annonce.setParticulier(particulier);
		annonce.setIdAnnonce(idAnnonce);

		ap.saveAndFlush(annonce);
		
		ArrayList<Annonce> listeAnnonce = (ArrayList<Annonce>) ap.findAll();
		
		mv.addObject("listeAnn",listeAnnonce);
		mv.addObject("particulier",particulier);
				
		mv.setViewName("menuParticulier");
		
		return mv;
	}
	
	@GetMapping(value = "/suppressionAnnonce")
	public ModelAndView supprimerAnnonce(ModelAndView mv, @RequestParam(value = "id") String id) {
		
		
		ap.deleteById(Integer.parseInt(id));
		
		ArrayList<Annonce> liste = (ArrayList<Annonce>) ap.findAll();

		for (Annonce ann : liste) {

			System.out.println(ann);

		}

		mv.addObject("listeAnn", liste);
		
		mv.setViewName("listeAnnonce");
		
		return mv;
	}
	
	
	@GetMapping(value = "/listerAnnonceLivreur")
	public ModelAndView listeAnnonceLivreur(ModelAndView mv) {

		ArrayList<Annonce> liste = (ArrayList<Annonce>) ap.findAll();

		for (Annonce ann : liste) {

			System.out.println(ann);

		}

		mv.addObject("listeAnn", liste);

		mv.setViewName("listeAnnonce");

		return mv;
	}
	
	@GetMapping(value = "/voirAnnonce")
	public ModelAndView listeAnnonceParticulier(ModelAndView mv) {

		ArrayList<Annonce> liste = (ArrayList<Annonce>) ap.findAll();

		mv.addObject("listeAnn", liste);


		mv.setViewName("listeAnnoncesTotale");

		return mv;
	}
	
	@GetMapping(value = "/voirAnnonceParticulier")
	public ModelAndView listeAnnonce(ModelAndView mv, @RequestParam(value = "id") Integer id) {
		
		Particulier particulier = pr.findById(id).get();

		mv.addObject("particulier", particulier);
		
		ArrayList<Annonce> liste = (ArrayList<Annonce>) ap.findAll();
		
		ArrayList<Annonce> listeAnnonceP = new ArrayList<Annonce>();
		
		for (Annonce annonce : liste) {
			if (annonce.getParticulier().getIdParticulier() == id) {
				
				listeAnnonceP.add(annonce);
				
			}
		}
		
		

		
		mv.addObject("listeAnn", listeAnnonceP);

		mv.setViewName("listeAnnonceParticulier");

		return mv;
	}


	
	
	
	@GetMapping(value = "/descriptionAnnonceLivreur")
	public ModelAndView redirectDescriptionAnnonce(ModelAndView mv, @RequestParam(value = "id") Integer id
			, @RequestParam(value = "idLivreur") Integer idLivreur) {
		

		Annonce annonce = ap.findById(id).get();
		Livreur livreur= lr.findById(idLivreur).get();
		
		System.out.println(livreur.getIdLivreur());
		System.out.println(annonce.getIdAnnonce());
		mv.addObject("annonce", annonce);
		mv.addObject("livreur", livreur);
		
		if (annonce.getIsActive().equals("Valide")) {
			mv.setViewName("descriptionAnnonce");
		}
		else if (annonce.getIsActive().equals("En Cours") & annonce.getId_LivreurEnCharge() == idLivreur ) {
			mv.setViewName("annonceDescriptionEnCoursLivreur");
		}
		else if (annonce.getIsActive().equals("En Cours") & annonce.getId_LivreurEnCharge() != idLivreur) {
			Livreur livreurAnnonce= lr.findById(annonce.getId_LivreurEnCharge()).get();
			mv.addObject("livreurAnnonce", livreurAnnonce);
			mv.setViewName("descriptionAnnonceEnCours");
		}
		else if (annonce.getIsActive().equals("Effectue")) {
			mv.setViewName("descriptionAnnonceDone");
		}
		else {
			mv.setViewName("menuLivreur");
		}
		
		return mv;
	}
	
	@GetMapping(value = "/descriptionAnnonceParticulier")
	public ModelAndView redirectDescriptionAnnonceParticulier(ModelAndView mv, @RequestParam(value = "id") Integer id
			, @RequestParam(value = "idParticulier") Integer idParticulier) {
		

		Annonce annonce = ap.findById(id).get();
		Particulier particulier= pr.findById(idParticulier).get();
		

		mv.addObject("annonce", annonce);
		mv.addObject("particulier", particulier);
		
		if (annonce.getIsActive().equals("Valide")) {
			mv.setViewName("descriptionAnnonceParticulier");
		}
		else if (annonce.getIsActive().equals("En Cours")) {
			mv.setViewName("annonceDescriptionEnCours");
		}
		
		else if (annonce.getIsActive().equals("Effectue")) {
			mv.setViewName("descriptionAnnonceDone");
		}
		else {
			mv.setViewName("menuParticulier");
		}
		
		return mv;
	}
	
	
	
	@PostMapping(value = "/prendreAnnonce")
	public ModelAndView prendreAnnonce (ModelAndView mv, @RequestParam(value = "idAnnonce") Integer id,
	@RequestParam(value = "idLivreur") Integer idLivreur) {
		
		
		Annonce annonce = ap.findById(id).get();
		Livreur livreur= lr.findById(idLivreur).get();
		
		annonce.setId_LivreurEnCharge(idLivreur);
		annonce.setIsActive("En Cours");
		ap.saveAndFlush(annonce);
		
		mv.addObject("livreur", livreur);
		
		ArrayList<Annonce> liste = (ArrayList<Annonce>) ap.findAll();
		
		mv.addObject("listeAnn", liste);		
		mv.setViewName("menuLivreur");
		
		return mv;
	}
	
	@GetMapping(value = "/retourDashboardParticulier")
	public ModelAndView retourDashboardParticulier(ModelAndView mv, @RequestParam(value = "id") Integer id) {
		
		Particulier particulier = pr.findById(id).get();

		mv.addObject("particulier", particulier);
		
		ArrayList<Annonce> liste = (ArrayList<Annonce>) ap.findAll();
		
		ArrayList<Annonce> listeAnnonceP = new ArrayList<Annonce>();
		
		for (Annonce annonce : liste) {
			if (annonce.getParticulier().getIdParticulier() == id & annonce.getParticulier().getIdParticulier() != null) {
				
				listeAnnonceP.add(annonce);
				
			}
		}
		
		

		
		mv.addObject("listeAnn", listeAnnonceP);

		mv.setViewName("menuParticulier");

		return mv;
	}
	
	@GetMapping(value = "/voirAnnonceByNom")
	public ModelAndView retourDashboardParticulierByNom(ModelAndView mv, @RequestParam(value = "id") Integer id) {
		
		Particulier particulier = pr.findById(id).get();

		mv.addObject("particulier", particulier);
		
		ArrayList<Annonce> liste = (ArrayList<Annonce>) ap.findAll(Sort.by("nomAnnonce").ascending());

		
		ArrayList<Annonce> listeAnnonceP = new ArrayList<Annonce>();
		
		for (Annonce annonce : liste) {
			if (annonce.getParticulier().getIdParticulier() == id & annonce.getParticulier().getIdParticulier() != null) {
				
				listeAnnonceP.add(annonce);
				
			}
		}
		
		

		
		mv.addObject("listeAnn", listeAnnonceP);

		mv.setViewName("menuParticulier");

		return mv;
	}
	@GetMapping(value = "/voirAnnonceByNomDec")
	public ModelAndView retourDashboardParticulierByNomDec(ModelAndView mv, @RequestParam(value = "id") Integer id) {
		
		Particulier particulier = pr.findById(id).get();

		mv.addObject("particulier", particulier);
		
		ArrayList<Annonce> liste = (ArrayList<Annonce>) ap.findAll(Sort.by("nomAnnonce").descending());

		
		ArrayList<Annonce> listeAnnonceP = new ArrayList<Annonce>();
		
		for (Annonce annonce : liste) {
			if (annonce.getParticulier().getIdParticulier() == id & annonce.getParticulier().getIdParticulier() != null) {
				
				listeAnnonceP.add(annonce);
				
			}
		}
		
		

		
		mv.addObject("listeAnn", listeAnnonceP);

		mv.setViewName("menuParticulier");

		return mv;
	}
	
	@PostMapping(value = "/abandonAnnonce")
	public ModelAndView abandonAnnonce (ModelAndView mv, @RequestParam(value = "idAnnonce") Integer id,
	@RequestParam(value = "idLivreur") Integer idLivreur) {
		
		
		Annonce annonce = ap.findById(id).get();
		Livreur livreur= lr.findById(idLivreur).get();
		
		annonce.setId_LivreurEnCharge(null);
		annonce.setIsActive("Valide");
		ap.saveAndFlush(annonce);
		
		mv.addObject("livreur", livreur);
		
		ArrayList<Annonce> liste = (ArrayList<Annonce>) ap.findAll();
		
		mv.addObject("listeAnn", liste);		
		mv.setViewName("menuLivreur");
		
		return mv;
	}
	
	@PostMapping(value = "/valideAnnonce")
	public ModelAndView valideAnnonce (ModelAndView mv, @RequestParam(value = "idAnnonce") Integer id,
	@RequestParam(value = "idLivreur") Integer idLivreur) {
		
		
		Annonce annonce = ap.findById(id).get();
		Livreur livreur= lr.findById(idLivreur).get();
		
		annonce.setIsActive("Effectue");
		ap.saveAndFlush(annonce);
		Float somme = annonce.getPrix_total();
		livreur.setArgent(livreur.getArgent() + somme);
		lr.saveAndFlush(livreur);
		
		mv.addObject("livreur", livreur);
		
		ArrayList<Annonce> liste = (ArrayList<Annonce>) ap.findAll();
		
		mv.addObject("listeAnn", liste);		
		mv.setViewName("menuLivreur");
		
		return mv;
	}
	
	
	
		
//	public void tuvasrepondreWlh() {
//		String username = "cda20108afpa@gmail.com";
//		String password = "Cda20108.";
//		Properties props = new Properties();
//		props.put("mail.smtp.auth", "true");
//		props.put("mail.smtp.starttls.enable", "true");
//		props.put("mail.smtp.host", "smtp.gmail.com");
//		props.put("mail.smtp.port", "587");
//		Session session = Session.getInstance(props, new Authenticator() {
//			protected PasswordAuthentication getPasswordAuthentication() {
//				return new PasswordAuthentication(username, password);
//			}
//		});
//		Message message = new MimeMessage(session);
//		try {
//			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("berkat.n@hotmail.com"));
//			message.setSubject("Bah alors on veut pas donner son mail ? ");
//			message.setText("REPOND REPOND REPOND REPOND REPOND REPOND REPOND REPOND REPOND REPOND REPOND REPOND REPOND REPOND REPOND REPOND REPOND REPOND REPOND REPOND ");
//			
//			
//			Transport.send(message);
//		} catch (AddressException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (MessagingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//	}
}
