<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Annonce en cours</title>
</head>
<body>
	<h1>ANNONCE EN COURS </h1>
	<p>Titre : ${ annonce.nomAnnonce}</p>
	<p>Ville depart : ${ annonce.adresseDepart.nomRue} ${ annonce.adresseDepart.codePostal} ${ annonce.adresseDepart.ville}</p>
	<p>Ville Arrivee : ${ annonce.adresseArrive.nomRue} ${ annonce.adresseArrive.codePostal} ${ annonce.adresseArrive.ville}</p>
	<p>Taille du colis : ${ annonce.taille_colis} cm</p>
	<p>Prix : ${ annonce.prix_km}</p>
	<p>Date de livraison : ${ annonce.date_limite}</p>
	<p>Description : ${ annonce.descriptif}</p>
	<p>Etat : ${ annonce.isActive} par ${ livreurAnnonce.nom} </p>
</body>
</html>