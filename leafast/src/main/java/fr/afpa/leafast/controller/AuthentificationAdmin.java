package fr.afpa.leafast.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import fr.afpa.beans.Admin;
import fr.afpa.beans.Annonce;
import fr.afpa.beans.Livreur;
import fr.afpa.beans.Particulier;
import fr.afpa.repositories.dao.AdminRepository;
import fr.afpa.repositories.dao.AnnonceRepository;
import fr.afpa.repositories.dao.LivreurRepository;
import fr.afpa.repositories.dao.ParticulierRepository;

@Controller
@SessionAttributes("admin")
public class AuthentificationAdmin {

	@Autowired
	AnnonceRepository ap;

	@Autowired
	AdminRepository ar;

	@Autowired
	ParticulierRepository pr;

	@Autowired
	LivreurRepository lr;

	@PostMapping(value = "/singinAdmin")
	public String singIn(Model model, @RequestParam(value = "login") String login,
			@RequestParam(value = "mdp") String mdp) {

		Integer nbrUser = 0;
		Integer nbrPar = 0;
		Integer nbrLiv = 0;
		Integer nbrAnn = 0;

		ArrayList<Admin> listeAdmin = (ArrayList<Admin>) ar.findAll();

		if (listeAdmin != null) {
			for (Admin admin : listeAdmin) {
				if (admin.getCompte().getLogin().equals(login) && BCrypt.checkpw(mdp, admin.getCompte().getMdp())) {
					model.addAttribute("admin", admin);

					ArrayList<Annonce> listeAnnonce = (ArrayList<Annonce>) ap.findAll();

					model.addAttribute("listeAnn", listeAnnonce);

					ArrayList<Particulier> listeParticulier = (ArrayList<Particulier>) pr.findAll();

					for (int i = 0; i < listeParticulier.size(); i++) {
						nbrPar++;
					}

					ArrayList<Livreur> listeLivreur = (ArrayList<Livreur>) lr.findAll();

					model.addAttribute("listeLiv", listeLivreur);

					for (int i = 0; i < listeLivreur.size(); i++) {
						nbrLiv++;
					}

					nbrUser = nbrPar + nbrLiv;

					ArrayList<Annonce> nbrAnnonces = (ArrayList<Annonce>) ap.findAll();

					for (int i = 0; i < nbrAnnonces.size(); i++) {
						nbrAnn++;
					}

					model.addAttribute("nbrUser", nbrUser);
					model.addAttribute("nbrPar", nbrPar);
					model.addAttribute("nbrLiv", nbrLiv);
					model.addAttribute("nbrAnn", nbrAnn);

					return "menuAdmin";
				}
			}
		}

		return "home";
	}

	@GetMapping(value = "/singoutAdmin")
	public String singOut(HttpSession session, SessionStatus status) {

		session.removeAttribute("admin");
		status.setComplete();

		return "connexionAdmin";
	}
}
