package fr.afpa.leafast.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.Livreur;
import fr.afpa.beans.Particulier;
import fr.afpa.repositories.dao.AnnonceRepository;
import fr.afpa.repositories.dao.LivreurRepository;
import fr.afpa.repositories.dao.ParticulierRepository;

@Controller
@SessionAttributes("livreur")
public class AuthentificationLivreur {
	@Autowired
	LivreurRepository lr;
	@Autowired
	AnnonceRepository ap;
	@Autowired
	ParticulierRepository pr;

	@GetMapping(value = "/singin2")
	public String singIn(Model model, @RequestParam(value = "login") String login,
			@RequestParam(value = "mdp") String mdp) {

		ArrayList<Livreur> listeLivreur = (ArrayList<Livreur>) lr.findAll();

		if (listeLivreur != null) {
			for (Livreur livreur : listeLivreur) {
				if (livreur.getCompte().getLogin().equals(login) && BCrypt.checkpw(mdp, livreur.getCompte().getMdp())) {
					model.addAttribute("livreur", livreur);
					
					ArrayList<Annonce> listeAnnonce = (ArrayList<Annonce>) ap.findAll();
					
					ArrayList<Annonce> listeAnnonceP = new ArrayList<Annonce>();
					
					int nbrAnnonce = 0;
					int nbrAnnonceEnCours = 0;
					for (Annonce annonce : listeAnnonce) {
						nbrAnnonce += 1;
						if (annonce.getId_LivreurEnCharge() == livreur.getIdLivreur()) {
							
							listeAnnonceP.add(annonce);
							
						}
						if (annonce.getId_LivreurEnCharge() == livreur.getIdLivreur() & annonce.getIsActive().equals("En Cours")) {
							
							nbrAnnonceEnCours += 1;
							
						}
					}
					
					ArrayList<Particulier> listeparticulier = (ArrayList<Particulier>) pr.findAll();
					
					int nbrParticulier = 0;
					for (Particulier particulier : listeparticulier) {
						nbrParticulier += 1;
					
					}
					
					model.addAttribute("nbrAnnonceEnCours",nbrAnnonceEnCours);
					model.addAttribute("nbrAnnonce",nbrAnnonce);
					model.addAttribute("nbrParticulier",nbrParticulier);
					model.addAttribute("listeAnn",listeAnnonceP);
					return "menuLivreur";
				}
			}
		}

		return "home";

	}

	@GetMapping(value = "/singout2")
	public String singOut(HttpSession session, SessionStatus status) {

		session.removeAttribute("livreur");
		status.setComplete();

		return "redirect:/";
	}

}
