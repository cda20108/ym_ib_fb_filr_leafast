<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Modifier ses informations</title>
</head>
<body>

	<form action="modifierLivreur" method="post">
	
		<input type="text" value="${ particulier.idParticulier }" name="idL" hidden="true">

		<input type="text" value="${ livreur.nom }" name="nom" required pattern='[A-z]{1,40}'>
		<input type="text" value="${ livreur.prenom }" name="prenom" required
					pattern='[A-z]{1,40}'>
		<input type="email" value="${ livreur.mail }" name="mail" required pattern='{5,50}'>
		<input type="tel" value="${ livreur.tel }" name="tel" required pattern='[0-9]{10}'>
		<input type="date" value="${ livreur.dateNaissance }" name="date" required>
		<input type="text" value="${ livreur.note }" name="note" readonly>
		<input type="text" value="${ livreur.nbrNote }" name="nbrNote" readonly>
		
		<input type="text" value="${ livreur.vehicule.idVehicule }" name="idV" hidden="true">
		
		<select name="typeVehicule" required> 
   			<label>Quelle est votre type de v�hicule ?<label>
   			<option>Electrique</option>
   			<option>Essence</option>
  			<option>Diesel</option>
		</select>
		
		<input type="text" value="${ livreur.vehicule.marqueVehicule }" name="marqueVehicule" required pattern='[A-z]{1,40}'>
		<input type="text" value="${ livreur.vehicule.modelVehicule }" name="modelVehicule" required pattern='[A-z\ ][0-9]){1,40}'>
		<input type="text" value="${ livreur.vehicule.emission }" name="emission" required pattern='[0-9]{1,3}'>
		<input type="text" value="${ livreur.vehicule.plaque }" name="plaque" required>
		
		<input type="text" value="${ livreur.compte.idCompte }" name="idC" hidden="true">
		
		<input type="text" value="${ livreur.compte.login }" name="login" readonly>
		<input type="password" value="${ livreur.compte.mdp }" name="mdp" required pattern='{5,20}'>

		<button type="submit">VALIDER</button>

	</form>
	
	<a href="singout">SE DECONNECTER</a>

</body>
</html>