<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/animate.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/style.css">

<!-- Google Fonts -->
<link
	href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700|Lato:400,100,300,700,900'
	rel='stylesheet' type='text/css'>



<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<meta charset="ISO-8859-1">
<title>Inscription</title>
</head>
<body>

	

	<form action="inscriptionL" method="post">
	<div class="container">
		<div class="top">
				<div class="logo">
					<img src="${pageContext.request.contextPath}/resources/images/logo_transparent.png" width="250" height="250" alt="">
				</div>
				<h1 id="title" class="hidden">
					<span id="logo"><span>Une livraison plus sur pour vous et l'environnement</span></span>
				</h1>
			</div>
			<div class="login-box animated fadeInUp">		
					
			<div class="box-header">
				<h2>Inscription Livreur</h2>
			</div>
			<br />
			<label for="username">Nom</label> <br />
			<input type="text" placeholder="Nom" name="nom" required pattern='[A-z]{1,40}'>
			<br />
			<label for="username">Prenom</label> <br />
			<input type="text" placeholder="Prenom" name="prenom" required
						pattern='[A-z]{1,40}'>
						<br />
			<label for="username">E-mail</label> <br />
			<input type="email" placeholder="Mail" name="mail" required pattern='{5,50}'>
			<br />
			<label for="username">Telephone </label> <br />
			<input type="tel" placeholder="T�l" name="tel" required pattern='[0-9]{10}'>
			<br />
			<label for="username">Date de naissance </label> <br />
			<input type="date" placeholder="Date de naissance" name="date" required>
			<br />
			
			<label for="username">Voiture </label> <br />
			<select name="typeVehicule" required>
	   			<label>Quelle est votre type de v�hicule ?<label>
	   			<option>Electrique</option>
	   			<option>Essence</option>
	  			<option>Diesel</option>
			</select>
			
			<br />	
			<label for="username">Marque du vehicule </label> <br />
			
			<select name="marqueVehciule" required>
			  
	   			<label>Choisissez votre marque ?<label>
	   			 <c:forEach items="${ carlist }" var="marque"  >
	   			<option  >${ marque }  </option>
	   		
	  			
	  			</c:forEach>
	  		
			</select>
			<br />	
			<label for="username">Modele du vehicule </label> <br />
			<input type="text" placeholder="Mod�le du vehicule" name="modelVehicule" required pattern='[A-z\ ][0-9]){1,40}'>
			<br />	
			<label for="username">Taux d'emission de CO� </label> <br />
			<input type="text" placeholder="Emission de gaz � effet de serre" name="emission" required pattern='[0-9]{1,3}'>
			<br />	
			<label for="username">Plaque d'immatriculation </label> <br />
			<input type="text" placeholder="Plaque d'immatriculation" name="plaque" required>
			<br />	
			<label for="username">Login </label> <br />
			<input type="text" placeholder="Login" name="login" required pattern='[A-z]{5,20}'>
			<br />	
			<label for="username">Mot de passe </label> <br />
			<input type="password" placeholder="Mot de passe" name="mdp" required pattern='{5,20}'>
			<br />
			<button type="submit">VALIDER</button>
			</div>
		</div>
	</form>
</body>
</html>