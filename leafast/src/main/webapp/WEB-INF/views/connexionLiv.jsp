<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Leafast</title>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/animate.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">
	
	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700|Lato:400,100,300,700,900' rel='stylesheet' type='text/css'>

	

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
</head>

<body>

	<form action="singin2" method="get" class="login">

	<div class="container">


		<div class="top">
				<div class="logo">
				   <img src="${pageContext.request.contextPath}/resources/images/logo_transparent.png" width="250" height="250" alt="">
				</div>
			<h1 id="title" class="hidden"><span id="logo" ><span>Une livraison plus sur pour vous et l'environnement</span></span></h1>
		</div>
		<div class="login-box animated fadeInUp">
			<div class="box-header">
				<h2>Connexion Livreur</h2>
			</div>
			<label for="username">Login</label>
			<br/>
			<input type="text"
			placeholder="Login" name="login" required pattern='[A-z]{5,20}'> 
			<br/>
			<label for="password">Mot de passe</label>
			<br/>
			<input type="password"
			placeholder="Mot de passe" name="mdp" required pattern='{5,20}'>
			<br/>
			<button type="submit">Connexion</button>
			<br/>
			
			
					<a href="inscription2"><p class="small">Pas de compte ? Devenir livreur !</p></a>

		</div>
	</div>
</form>
</body>

<script>
	$(document).ready(function () {
    	$('#logo').addClass('animated fadeInDown');
    	$("input:text:visible:first").focus();
	});
	$('#username').focus(function() {
		$('label[for="username"]').addClass('selected');
	});
	$('#username').blur(function() {
		$('label[for="username"]').removeClass('selected');
	});
	$('#password').focus(function() {
		$('label[for="password"]').addClass('selected');
	});
	$('#password').blur(function() {
		$('label[for="password"]').removeClass('selected');
	});
</script>
</html>