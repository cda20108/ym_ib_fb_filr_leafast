package fr.afpa.repositories.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.beans.Admin;

public interface AdminRepository extends JpaRepository<Admin, Integer>{

	
}
