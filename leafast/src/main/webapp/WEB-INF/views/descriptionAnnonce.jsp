<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Description Annonce</title>
</head>
<body>
	<p>Titre : ${ annonce.nomAnnonce}</p>
	<p>Ville depart : ${ annonce.adresseDepart.nomRue} ${ annonce.adresseDepart.codePostal} ${ annonce.adresseDepart.ville}</p>
	<p>Ville Arrivee : ${ annonce.adresseArrive.nomRue} ${ annonce.adresseArrive.codePostal} ${ annonce.adresseArrive.ville}</p>
	<p>Taille du colis : ${ annonce.taille_colis} cm</p>
	<p>Prix : ${ annonce.prix_km}</p>
	<p>Date de livraison : ${ annonce.date_limite}</p>
	<p>Description : ${ annonce.descriptif}</p>
	<p>Etat : ${ annonce.isActive}</p>
	
	<form action="prendreAnnonce" method="post">
	
	<input type="text" value="${ livreur.idLivreur}" name="idLivreur" hidden="true">
	<input type="text" value="${ annonce.idAnnonce}" name="idAnnonce" hidden="true">
	<button type="submit">Prendre cette annonce </button>
	</form>
	
</body>
</html>