package fr.afpa.leafast.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.Livreur;
import fr.afpa.beans.Particulier;
import fr.afpa.repositories.dao.AnnonceRepository;
import fr.afpa.repositories.dao.LivreurRepository;
import fr.afpa.repositories.dao.ParticulierRepository;

@Controller
public class redirectLivreurController {
	@Autowired
	LivreurRepository lr;
	@Autowired
	AnnonceRepository ap;
	@Autowired
	ParticulierRepository pr;
	
	@GetMapping(value = "/listeAnnonceTotaleLivreur")
	public ModelAndView redirectFullListeAnnonceParticulier(ModelAndView mv, @RequestParam(value = "id") Integer idLivreur) {
		Livreur livreur= lr.findById(idLivreur).get();
		
		ArrayList<Annonce> liste = (ArrayList<Annonce>) ap.findAll();
		
		ArrayList<Particulier> listeparticulier = (ArrayList<Particulier>) pr.findAll();
		int nbrParticulier = 0;
		for (Particulier particulier : listeparticulier) {
			nbrParticulier += 1;
		
		}
		
		
		mv.addObject("nbrParticulier",nbrParticulier);
		mv.addObject("listeAnn", liste);
		mv.addObject("livreur", livreur);
		mv.setViewName("listeAnnonceTotaleLivreur");
		return mv;
	}
	
	@GetMapping(value = "/retourDashboard")
	public ModelAndView redirectDashboardLivreur(ModelAndView mv, @RequestParam(value = "id") Integer idLivreur) {
		Livreur livreur= lr.findById(idLivreur).get();
		mv.addObject("livreur", livreur);
		
		
		ArrayList<Annonce> listeAnnonce = (ArrayList<Annonce>) ap.findAll();
		
		ArrayList<Annonce> listeAnnonceP = new ArrayList<Annonce>();
		
		int nbrAnnonce = 0;
		int nbrAnnonceEnCours = 0;
		for (Annonce annonce : listeAnnonce) {
			nbrAnnonce += 1;
			if (annonce.getId_LivreurEnCharge() == livreur.getIdLivreur()) {
				
				listeAnnonceP.add(annonce);
				
			}
			if (annonce.getId_LivreurEnCharge() == livreur.getIdLivreur() & annonce.getIsActive().equals("En Cours")) {
				
				nbrAnnonceEnCours += 1;
				
			}
		}
		
		ArrayList<Particulier> listeparticulier = (ArrayList<Particulier>) pr.findAll();
		
		int nbrParticulier = 0;
		for (Particulier particulier : listeparticulier) {
			nbrParticulier += 1;
		
		}
		
			mv.addObject("nbrAnnonceEnCours",nbrAnnonceEnCours);
			mv.addObject("nbrAnnonce",nbrAnnonce);
			mv.addObject("nbrParticulier",nbrParticulier);
			mv.addObject("listeAnn",listeAnnonceP);
			
			mv.setViewName("menuLivreur");
			return mv;
		}
	
	@GetMapping(value = "/parametreLivreur")
	public ModelAndView redirectParametreLivreur(ModelAndView mv, @RequestParam(value = "id") Integer idLivreur) {
		Livreur livreur= lr.findById(idLivreur).get();
		
		
		mv.addObject("livreur", livreur);
		mv.setViewName("ParametreLivreur");
		return mv;
	}
	
	@GetMapping(value = "/livreurNousContacter")
	public ModelAndView livreurNousContacter(ModelAndView mv, @RequestParam(value = "id") Integer idLivreur) {
		Livreur livreur= lr.findById(idLivreur).get();
		
		
		mv.addObject("livreur", livreur);
		mv.setViewName("livreurNousContacter");
		return mv;
	}
}
