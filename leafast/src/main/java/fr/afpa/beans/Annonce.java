package fr.afpa.beans;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@ToString
@Entity

public class Annonce {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idAnnonce;

	@NonNull
	private String nomAnnonce;

	@NonNull
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_adresseDepart")
	private Adresse adresseDepart;
	
	@NonNull
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_adresseArrive")
	private Adresse adresseArrive;

	@NonNull
	private String taille_colis;

	@NonNull
	private String prix_km;
	
	@NonNull
	private Float km;

	@NonNull
	private Float prix_total;
	
	@NonNull
	private LocalDate date_limite;
	
	@NonNull
	private String descriptif;
	
	@NonNull
	private String isActive;
	
	private Integer id_LivreurEnCharge;
	
	private Float note;
	
	private String commentaire;
	
	@ManyToOne
	@JoinColumn(name = "id_particulier")
	private Particulier particulier;

}
